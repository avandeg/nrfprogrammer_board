EESchema Schematic File Version 4
LIBS:programmer-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "BLE_Programmer"
Date ""
Rev "A"
Comp "CoreKinect"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J1
U 1 1 5BA31D3F
P 1900 1600
F 0 "J1" H 1950 2217 50  0000 C CNN
F 1 "jLink" H 1950 2126 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x10_P2.54mm_Vertical" H 1900 1600 50  0001 C CNN
F 3 "" H 1900 1600 50  0001 C CNN
	1    1900 1600
	1    0    0    -1  
$EndComp
Text Label 950  1200 0    50   ~ 0
V_3p3
Wire Wire Line
	1700 1200 950  1200
Wire Notes Line
	1650 1550 1550 1550
Wire Notes Line
	1550 1550 1550 1750
Wire Notes Line
	1550 1750 1650 1750
Wire Notes Line
	1650 1750 1650 2200
Wire Notes Line
	1650 2200 2250 2200
Wire Notes Line
	2250 2200 2250 900 
Wire Notes Line
	2250 900  1650 900 
Wire Notes Line
	1650 900  1650 1550
Wire Wire Line
	1700 1500 950  1500
Text Label 950  1500 0    50   ~ 0
SWDIO
Wire Wire Line
	1700 1600 950  1600
Text Label 950  1600 0    50   ~ 0
SWCLK
Wire Wire Line
	1700 1900 950  1900
Text Label 950  1900 0    50   ~ 0
RESET_B
Text Label 2650 1300 0    50   ~ 0
GND
Wire Wire Line
	2200 1300 2800 1300
$Comp
L Connector:Conn_01x05_Male J3
U 1 1 5BA3232C
P 4100 1650
F 0 "J3" H 4000 1700 50  0000 R CNN
F 1 "pogo_pins_5" H 4000 1600 50  0000 R CNN
F 2 "connectors:JST_PH_B5B-PH-K_1x05_P2.00mm_Vertical_pogo" H 4100 1650 50  0001 C CNN
F 3 "" H 4100 1650 50  0001 C CNN
	1    4100 1650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3900 1850 3450 1850
Text Label 3450 1850 0    50   ~ 0
V_3p3
Wire Wire Line
	3900 1750 3450 1750
Text Label 3450 1750 0    50   ~ 0
GND
Wire Wire Line
	3900 1650 3450 1650
Text Label 3450 1650 0    50   ~ 0
RESET_B
Wire Wire Line
	3900 1550 3450 1550
Text Label 3450 1550 0    50   ~ 0
SWCLK
Wire Wire Line
	3900 1450 3450 1450
Text Label 3450 1450 0    50   ~ 0
SWDIO
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5BA328C9
P 2550 2600
F 0 "J2" H 2550 2750 50  0000 C CNN
F 1 "Power_in" H 2600 2400 50  0000 C CNN
F 2 "Connector_Molex:Molex_KK-254_AE-6410-02A_1x02_P2.54mm_Vertical" H 2550 2600 50  0001 C CNN
F 3 "" H 2550 2600 50  0001 C CNN
	1    2550 2600
	1    0    0    -1  
$EndComp
Text Label 3200 2600 0    50   ~ 0
V_3p3
Wire Wire Line
	2750 2600 3400 2600
Text Label 3200 2700 0    50   ~ 0
GND
Wire Wire Line
	2750 2700 3400 2700
NoConn ~ 1700 1300
NoConn ~ 1700 1400
NoConn ~ 1700 1700
NoConn ~ 1700 1800
NoConn ~ 1700 2000
NoConn ~ 1700 2100
NoConn ~ 2200 2100
NoConn ~ 2200 2000
NoConn ~ 2200 1900
NoConn ~ 2200 1800
NoConn ~ 2200 1700
NoConn ~ 2200 1600
NoConn ~ 2200 1500
NoConn ~ 2200 1400
NoConn ~ 2200 1200
$Comp
L Mechanical:MountingHole_Pad MH1
U 1 1 5BA34C80
P 1200 2850
F 0 "MH1" H 1300 2901 50  0000 L CNN
F 1 "MountingHole_Pad" H 1300 2810 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 1200 2850 50  0001 C CNN
F 3 "~" H 1200 2850 50  0001 C CNN
	1    1200 2850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH2
U 1 1 5BA34DA2
P 1200 3150
F 0 "MH2" H 1300 3201 50  0000 L CNN
F 1 "MountingHole_Pad" H 1300 3110 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 1200 3150 50  0001 C CNN
F 3 "~" H 1200 3150 50  0001 C CNN
	1    1200 3150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH3
U 1 1 5BA34E0E
P 1200 3450
F 0 "MH3" H 1300 3501 50  0000 L CNN
F 1 "MountingHole_Pad" H 1300 3410 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 1200 3450 50  0001 C CNN
F 3 "~" H 1200 3450 50  0001 C CNN
	1    1200 3450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH4
U 1 1 5BA34E76
P 1200 3750
F 0 "MH4" H 1300 3801 50  0000 L CNN
F 1 "MountingHole_Pad" H 1300 3710 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 1200 3750 50  0001 C CNN
F 3 "~" H 1200 3750 50  0001 C CNN
	1    1200 3750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
